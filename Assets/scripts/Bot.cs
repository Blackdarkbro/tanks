﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : MonoBehaviour
{

    [SerializeField] float moveSpeed;
    [SerializeField] float rotSpeed;

    [SerializeField] GameObject turret;
    [SerializeField] GameObject shell;

    private bool canShoot = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Vector3 otherPosition = other.transform.position;

            float distance = Vector3.Distance(otherPosition, transform.position);
            Vector3 relativePos = otherPosition - transform.position;
            Quaternion newRot = Quaternion.LookRotation(relativePos);

            transform.rotation = Quaternion.Slerp(transform.rotation, newRot, Time.deltaTime * rotSpeed);
            turret.transform.rotation = Quaternion.Slerp(turret.transform.rotation, newRot,Time.deltaTime * rotSpeed * 2);

            if (distance > 130)
            {
                transform.position = Vector3.Lerp(transform.position, otherPosition, Time.deltaTime * moveSpeed);
            }
            else
            {
                transform.position = transform.position;
            }
        }

        TakeAim();
    }

    IEnumerator BotShoot()
    {
        canShoot = false;
        Vector3 forwardOfStvol = turret.transform.position + turret.transform.TransformDirection(0, 2f, 33f);

        GameObject newShell1 = Instantiate(shell, forwardOfStvol, turret.transform.rotation);
        GameObject newShell2 = Instantiate(shell, forwardOfStvol + transform.TransformDirection(Vector3.right * 2f), turret.transform.rotation);

        yield return new WaitForSeconds(1f);
        canShoot = true;
    }

    void TakeAim()
    {
        RaycastHit hit;
        if (Physics.Raycast(turret.transform.position, turret.transform.TransformDirection(Vector3.forward), out hit))
        {
            if (hit.transform.CompareTag("Player") && canShoot)
            {
                StartCoroutine(BotShoot());
            }
        }
    }
}