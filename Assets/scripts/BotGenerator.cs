﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotGenerator : MonoBehaviour
{
    [SerializeField] GameObject gameController;
    [SerializeField] Text botCountText;

    [SerializeField] GameObject bot;
    [SerializeField] List<GameObject> spawns = new List<GameObject>();

    [SerializeField] int botCount;
    [SerializeField] int generateCount;
    [SerializeField] float botSpawnTime;

    private List<GameObject> bots = new List<GameObject>();
    private float time = 0;

    void Start()
    {
        generateCount = botCount;
        int lastSpawn = spawns.Count - 1;

        for (int i = 0; i < 3; i++)
        {
            bots.Add(Instantiate(bot, spawns[lastSpawn--].transform.position, Quaternion.identity));
            botCount--;
        }
    }

    void Update()
    {
        time += Time.deltaTime;
        if (botCount > 0)
        {
            if (time >= botSpawnTime)
            {
                int randonSpawn = Random.Range(0, spawns.Count);
                bots.Add(Instantiate(bot, spawns[randonSpawn].transform.position, Quaternion.identity));

                time = 0;
                botCount--;
            }
        }
     

        // check for destroyed bots
        foreach (var item in bots)
        {
            if (item.activeSelf == false)
            {
                bots.Remove(item);
                Destroy(item);
                generateCount--;
            }
        }

        if (generateCount <= 0)
        {
            gameController.GetComponent<GameController>().WinGame();
        }

        botCountText.text = "Осталось убить: " + generateCount.ToString();
    }
}
