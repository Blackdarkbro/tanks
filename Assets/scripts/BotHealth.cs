﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BotHealth : MonoBehaviour
{
    [SerializeField] GameObject bigExplosionEffect;
    [SerializeField] float damage;
    [SerializeField] AudioClip boomSound;

    private float botHP = 100;

    void Update()
    {
        if (botHP <= 0)
        {
            GameObject boom = Instantiate(bigExplosionEffect, transform.position + transform.TransformDirection(Vector3.up * 15f), transform.rotation);
            boom.transform.localScale *= 10;

            Destroy(boom, 1.5f);
            gameObject.SetActive(false);

            gameObject.AddComponent<AudioSource>().clip = boomSound;
            gameObject.GetComponent<AudioSource>().PlayOneShot(boomSound);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("shell"))
        {
            botHP -= damage;
        }
    }
}
