﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField] GameObject menuCanvas;
    [SerializeField] GameObject gameoverCanvas;

    [SerializeField] Text gameOverText;
    [SerializeField] GameObject menuBackground;

    public float timer;
    public bool ispuse;
    public bool guipuse;

    private bool isGameOver = false;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = timer;
        if (Input.GetKeyDown(KeyCode.Escape) && ispuse == false && isGameOver == false)
        {
            menuCanvas.SetActive(true);
            Cursor.visible = true;
            ispuse = true;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && ispuse == true && isGameOver == false)
        {
            menuCanvas.SetActive(false);
            Cursor.visible = false;
            ispuse = false;
        }

        if (ispuse == true)
        {
            timer = 0;
            guipuse = true;

        }
        else if (ispuse == false)
        {
            timer = 1f;
            guipuse = false;

        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        isGameOver = false;
    }

    public void WinGame()
    {
        menuBackground.GetComponent<Image>().color = new Color(0, 108, 47, 214);
        gameOverText.text = "Победа";
        isGameOver = true;

        Invoke("ShowGameOverPanel", 1.5f);
    }

    public void LooseGame()
    {
        menuBackground.GetComponent<Image>().color = new Color(108, 1, 0, 214);
        gameOverText.text = "Вы проиграли";
        isGameOver = true;

        Invoke("ShowGameOverPanel", 1.5f);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    void ShowGameOverPanel()
    {
        ispuse = true;
        gameoverCanvas.SetActive(true);
        Cursor.visible = true;
    }
}
