﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankHealth : MonoBehaviour
{
    [SerializeField] GameObject gameController;

    [SerializeField] GameObject fireExplosion;
    [SerializeField] AudioClip boomSound;

    [SerializeField] float shieldRecoveryTime;
    [SerializeField] int damage;

    [SerializeField] Scrollbar shieldScroll;
    [SerializeField] GameObject shieldScrolldHandler;

    [SerializeField] Scrollbar HPScroll;
    [SerializeField] GameObject HPScrollHandler;

    private float tankHP = 100, shieldHP = 100;
    private float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        shieldScroll.size = shieldHP / 100;
        HPScroll.size = tankHP / 100;

        timer += Time.deltaTime;

        if (shieldHP <= 0)
        {
            shieldScrolldHandler.SetActive(false);
        } else
        {
            shieldScrolldHandler.SetActive(true);
        }

        if (timer >= shieldRecoveryTime)
        {
            shieldHP += 0.5f;
            if (shieldHP > 100) shieldHP = 100;
        }

        if (tankHP <= 0)
        {
            HPScrollHandler.SetActive(false);
            GameOver();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "shell")
        {
            if (shieldHP > 0)
            {
                shieldHP -= damage;
            }
            
            if (shieldHP <= 0)
            {
                tankHP -= damage;
                GameObject boom = Instantiate(fireExplosion, collision.transform.position, transform.rotation);
                boom.transform.localScale *= 3f;
                Destroy(boom, 1f);
            }
            timer = 0;
        }
    }

    void GameOver()
    {
        GameObject shassis = gameObject.transform.Find("shassis").gameObject;
        GameObject corpus = gameObject.transform.Find("corpus").gameObject;
        GameObject turret = gameObject.transform.Find("turret").gameObject;

        turret.AddComponent<Rigidbody>();
        turret.AddComponent<BoxCollider>();

        GameObject boom = Instantiate(fireExplosion, transform.position, transform.rotation);
        boom.transform.localScale *= 10f;
        Destroy(boom, 1f);

        gameObject.AddComponent<AudioSource>().clip = boomSound;
        gameObject.GetComponent<AudioSource>().PlayOneShot(boomSound);

        Destroy(shassis);
        Destroy(corpus);


        gameController.GetComponent<GameController>().LooseGame();
    }
}
