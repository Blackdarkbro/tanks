﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class panzerScript : MonoBehaviour
{
    [SerializeField] GameObject turret;
    [SerializeField] Camera camera;

    [SerializeField] Text tankText;
    [SerializeField] Text shellText;

    AudioSource motor;

    [SerializeField] float moveSpeed = 30f;
    [SerializeField] float turretSpeed = 150f;
    [SerializeField] float rotateSpeed = 40f;

    float rotateTurretX, rotateTurretY;
    Quaternion startTurretRotation;

    
    // Start is called before the first frame update
    void Start()
    {
        motor = GetComponent<AudioSource>();
        startTurretRotation = turret.transform.rotation;
     
    }
    private void Update()
    {
        TurretRotate();
        SoundPanzer();
    }

    private void FixedUpdate()
    {
        float rotateX = Input.GetAxis("Horizontal") * rotateSpeed * Time.deltaTime;
        float dz = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;

        transform.Translate(0, 0, dz);
        transform.Rotate(0, rotateX, 0);



    }

    void TurretRotate()
    {
        rotateTurretX += Input.GetAxis("Mouse X") * turretSpeed * Time.deltaTime;
        rotateTurretY += Input.GetAxis("Mouse Y") * turretSpeed * Time.deltaTime;

        rotateTurretX = Mathf.Clamp(rotateTurretX, -90, 90);
        rotateTurretY = Mathf.Clamp(rotateTurretY, -10, 10);

        Quaternion newTurretRotationdX = Quaternion.AngleAxis(rotateTurretX, Vector3.up);
        Quaternion newTurretRotationdY = Quaternion.AngleAxis(rotateTurretY, Vector3.right);

        turret.transform.localRotation = startTurretRotation * newTurretRotationdX * newTurretRotationdY; 
    }

    void SoundPanzer()
    {
        if (Input.GetKey(KeyCode.W))
        {
            if (motor.pitch <= 2)
            {
                motor.pitch += 0.005f;
            }
        }
        else
        {
            if (motor.pitch >= 1)
            {
                motor.pitch -= 0.005f;
            }
        }
    }

    public void ChangeTankSpeed(float value)
    {
        moveSpeed = value * 5;
        tankText.text = "Скорость танка: " + value;
    }
    public void ChangeShellSpeedText(float value)
    {
        shellText.text = $"Скорость снаряда: {value}";
    }

}
