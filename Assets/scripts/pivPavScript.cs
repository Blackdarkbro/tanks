﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pivPavScript : MonoBehaviour
{
    [SerializeField] GameObject shell;
    AudioSource rotSound;

    Quaternion startTurretRotation, oldTurretRot;
    // Start is called before the first frame update
    void Start()
    {
        rotSound = GetComponent<AudioSource>();
        oldTurretRot = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 firstShellPosition = transform.position + transform.TransformDirection(new Vector3(-2f, 2f, 33f));
            Vector3 secondShellPosition = transform.position + transform.TransformDirection(new Vector3(+2f, 0, 33f));

            Instantiate(shell, firstShellPosition, transform.rotation);
            Instantiate(shell, secondShellPosition, transform.rotation);
        }
        SoundTurret();
    }

    void SoundTurret()
    {
        Quaternion newRot = transform.rotation;
        if (newRot != oldTurretRot)
        {
            rotSound.volume = 1;
        } else
        {
            rotSound.volume = 0;
        }
        oldTurretRot = transform.rotation;
    }
}
