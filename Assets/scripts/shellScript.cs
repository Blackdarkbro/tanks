﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shellScript : MonoBehaviour
{
    [SerializeField] GameObject energyExplosion;
    [SerializeField] AudioClip bang;
    [SerializeField] float shellSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.TransformDirection(Vector3.forward * shellSpeed);
        Destroy(gameObject, 3f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        gameObject.GetComponent<LineRenderer>().enabled = false;
        gameObject.GetComponent<Collider>().enabled = false;

        GameObject boom = Instantiate(energyExplosion, transform.position, transform.rotation);
        Destroy(boom, 1f);

        if (collision.collider.tag == "aim")
        {
            StartCoroutine(Anigilyator(collision.gameObject));
        }
    }

    IEnumerator Anigilyator(GameObject target)
    {
        target.GetComponent<Renderer>().material.color = Color.red;

        target.AddComponent<AudioSource>().clip = bang;
        target.GetComponent<AudioSource>().PlayOneShot(bang);
        target.AddComponent<AudioSource>().volume = 0.3f;

        yield return new WaitForSeconds(.3f);
        Destroy(target, .7f);
    }

    public void ChangeShellSpeed(float value)
    {
        shellSpeed = value;
    }
}
